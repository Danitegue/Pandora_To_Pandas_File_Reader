# -*- coding: utf-8 -*-

__author__ = 'Daniel Santana'

#Pandora Analysis libraries for pandas.
#20180410
#---------------------------------------

import sys
import pandas
import datetime
import time
import numpy as np
import os
from copy import deepcopy

iso_date_str = "%Y%m%dT%H%M%SZ"
iso_date_str2 ="%Y%m%dT%H%M%S.%fZ"
#Keywords dict: identifiers of each pandora file column.
#Compatible with pan format as well as blick format.
keywords_pandora = {

    #Datetimes
    "UT_datetime":['UT date and time for center of measurement','UT date and time for beginning of measurement'],
    "UT_MJD2K":['Fractional days since 1-Jan-2000 midnight for center of measurement', 'Fractional days since 1-Jan-2000 midnight for beginning of measurement', 'Fractional days since 1-Jan-2000 UT midnight for center of measurement'], #Modified Julian Day, as GEOMS format.


    # Measurements info:
    "Routine": ['Two letter code of measurement routine'],
    "Routine_count": ['Routine count (1 for the first routine of the day, 2 for the second, etc.)'],
    "Repetition_count":['Repetition count (1 for the first set in the routine, 2 for the second, etc.)'],
    "Total_duration":['Total duration of measurement set in seconds','Total duration of measurement in seconds', 'Effective duration of measurement in seconds'], # (=#, if the line is a comment line)'],
    "Integration_time":['Integration time [ms]'],
    "Num_of_cycles":['Number of cycles'],
    "Num_of_bright_cycles": ["Number of bright count cycles"],
    "Num_of_dark_cycles": ['Number of dark count cycles'],
    "Filterwheel_1":['Position of filterwheel #1','Filterwheel_1', 'Effective position of filterwheel #1'], #, 0=filterwheel not used, 1-9 are valid positions
    "Filterwheel_2":['Position of filterwheel #2','Filterwheel_2', 'Effective position of filterwheel #2'], #, 0=filterwheel not used, 1-9 are valid positions
    "Saturation_index":["Saturation index"],
    "Target_distance":["Target distance [m]"],

    # Pointing info
    "Pointing_zenith":['Pointing zenith angle in degree'], # absolute or relative (see next column), 999=tracker not used'
    "Pointing_zenith_mode":['Zenith pointing mode'], # zenith angle is... 0=absolute, 1=relative to sun, 2=relative to moon']
    "Pointing_azimuth":['Pointing azimuth in degree'], #, increases clockwise, absolute (0=north) or relative (see next column), 999=tracker not used
    "Pointing_azimuth_mode":['Azimuth pointing mode'], #: like zenith angle mode but also fixed scattering angles relative to sun (3) or moon (4)
    "Solar_zenith_angle":["Solar zenith angle at the center-time of the measurement in degree", 'Solar zenith angle for center of measurement in degree'],# Column 6: Solar zenith angle at the center-time of the measurement in degree
    "Solar_azimuth_angle":["Solar azimuth at the center-time of the measurement in degree", 'Solar azimuth for center of measurement in degree'],# Column 7: Solar azimuth at the center-time of the measurement in degree, 0=north, increases clockwise
    "Lunar_zenith_angle":["Lunar zenith angle at the center-time of the measurement in degree",'Lunar zenith angle for center of measurement in degree'],# Column 8: Lunar zenith angle at the center-time of the measurement in degree
    "Lunar_azimuth_angle":["Lunar azimuth at the center-time of the measurement in degree", 'Lunar azimuth for center of measurement in degree'],# Column 9: Lunar azimuth at the center-time of the measurement in degree, 0=north, increases clockwise



    # Instrumental temperatures
    "Temp_electronics_board_1":['Temperature at electronics board [','Temperature at electronics board 1 ['], #°C], 999=no temperature signal
    "Temp_electronics_board_2":['Temperature at electronics board 2 ['], #°C], 999=no temperature signal
    "Temp_spec_control_1":['Spectrometer control temperature [','Spectrometer control temperature 1 ['], #°C], 999=no temperature signal
    "Temp_spec_control_2":['Spectrometer control temperature 2 ['], #°C], 999=no temperature signal
    "Temp_aux_1":['Auxiliary spectrometer temperature [', 'Auxiliary spectrometer temperature 1 ['], #°C], 999=no temperature signal
    "Temp_aux_2":['Auxiliary spectrometer temperature 2 ['], #°C], 999=no temperature signal
    "Temp_eff":["Effective temperature [","Wavelength effective temperature ["], #Effective temperature [°C], 999=no effective temperature given
    "Temp_detector_1":["Temperature at detector 1 [",], #Detector temperature [°C], 999=no detector temperature given
    "Temp_detector_2":["Temperature at detector 2 [",], #Detector temperature [°C], 999=no detector temperature given
    "Temp_spec_box":["Temperature inside spectrometer box [",], #°C], 999=no temperature signal


    # Instrumental sensors:
    "Hum_head":["Humidity in head sensor ["], #Humidity in head sensor [%], -9=no humidity signal
    "Temp_head":["Temperature in head sensor ["], #Temperature in head sensor [deg], 999=no temperature signal
    "Pres_head":["Pressure in head sensor ["], #Pressure in head sensor [hPa], -9=no pressure signal
    "Hum_spec_box":["Humidity inside spectrometer box ["], #Humidity inside spectrometer box [%], -9=no humidity signal


    #Raw counts:
    "Counts_scaled":['Mean over all cycles of raw counts for each pixel'],
    "Scale_factor":['Scale factor for output data',"Scale factor for data"], # (to obtain unscaled output devide data by this number)'],


    #Corrected data: Counts/s, Irradiance, Radiance
    "Corr_data_type":['Level 2 data type','Level 1 data type'], #Pan format, Blick Format. data are... 1=corrected count rate [s-1], 2=radiance [W/m2/nm/sr], 3=irradiance [W/m2/nm]
    "Corr_data_scaled":['Level 2 data for each pixel','Level 1 data for each pixel'], #Pan format, Blick Format


    #Raw counts or Corrected data uncertainties
    "Uncert_indicator": ['Uncertainty indicator'],
    "Uncert_meas":["Uncertainty of raw counts for each pixel divided by the square root of the number of cycles",'Uncertainty of level 1 data for each pixel','Uncertainty of level 2 data for each pixel'],
    "Uncert_instr":['Instrumental uncertainty of level 1 data for each pixel'],

    #Processing options and parameters: Raw Counts -> Corrected data
    "Data_processing_steps": ["Sum over 2^i with i being a level 1 to level 2 conversion step",'Sum over 2^i with i being a L0 to L1 conversion step'],
    "Data_processing_type_index": ["Data processing type index"],
    'Data_processing_dark_corr_meth': ['Dark correction method'],
    'CF_version': ['Calibration file version used','Calibration file version'],
    'CF_validity_date': ['Calibration file validity starting date'],

    #Processing results: Raw Counts -> Corrected data
    "Error_index":["Sum over 2^i with i being an error index","Sum over 2^i with i being a level 2 error index"],

    "Non_linearity_correction_max": ["Maximum non-linearity correction applied to the data [%]"],
    "Non_linearity_correction_min": ["Minimum non-linearity correction applied to the data [%]"],

    "Blind_and_oversampled_mean_bright":['Mean over blind and oversampled pixels in the bright counts,'],
    "Blind_and_oversampled_mean_bright_uncert":['Uncertainty of mean over blind and oversampled pixels in the bright counts'],
    "Blind_and_oversampled_mean_dark":['Mean over blind and oversampled pixels in the dark counts,'],
    "Blind_and_oversampled_mean_dark_uncert":['Uncertainty of mean over blind and oversampled pixels in the dark counts'],

    "Npix_DQ1sigma_not_overlap":['Number of pixels, where DQ1 sigma ranges of dark count and bright count do not overlap,'],
    "Npix_DQ2sigma_not_overlap":['Number of pixels, where DQ2 sigma ranges of dark count and bright count do not overlap,'],

    "Num_of_pix_where_higher_dark_than_bright":["Number of pixels, where dark count is higher than bright count"],
    "Num_of_pix_where_5_sigma_do_not_overlap":["Number of pixels, where 5-sigma ranges of dark count and bright count do not overlap"],

    "Highest_counts_pix_index":["Index of (not dead, blind, warm, or saturated) pixel with the highest corrected counts","Index of (not dead, blind, warm, or saturated) pixel with the highest counts"],
    "Highest_counts_pix_uncert_meas_brigth_meas_dark":["Uncertainty [%] of data from pixel with the highest counts based on measured bright count uncertainty and measured dark"],
    "Highest_counts_pix_uncert_meas_brigth_theoretical_dark":["Uncertainty [%] of data from pixel with the highest counts based on measured bright count uncertainty and theoretical dark"],
    "Highest_counts_pix_uncert_theoretical_brigth_meas_dark":["Uncertainty [%] of data from pixel with the highest counts based on theoretical bright count uncertainty and measured dark"],
    "Highest_counts_pix_uncert_theoretical_brigth_theoretical_dark":["Uncertainty [%] of data from pixel with the highest counts based on theoretical bright count uncertainty and theoretical dark"],

    "Signal_at_pix_closest_to_ref_wv":["Signal at pixel closest to reference wavelength"],# Column 60: Signal at pixel closest to reference wavelength

    #Dark mapping
    'Dark_map_index_of_regular_pixel_most_differed':['Index of regular pixel, where the dark map differed most from the measured dark count'],
    'Dark_map_diff_mean':['Mean difference dark map to measured dark count'],
    'Dark_map_diff_rms':['rms of difference dark map to measured dark count'],
    'Dark_map_diff_abs_in_std_deviations': ['Mean absolute difference dark map to measured dark count expressed in number of standard deviations'],
    'Dark_map_res_norm_rms_weighted':['Normalized rms of weighted fitting residuals for dark background fitting'],
    'Dark_map_res_rms_unweighted': ['rms of unweighted fitting residuals for dark background fitting'],
    'Dark_map_num_of_hot_pix_10s':['Number of hot pixels, where the dark map differs from the measured dark by more than 10 sigmas'],
    'Dark_map_num_of_reg_pix_10s':['Number of regular pixels, where the dark map differs from the measured dark by more than 10 sigmas'],
    'Dark_map_num_of_warm_pix_10s':['Number of warm pixels, where the dark map differs from the measured dark by more than 10 sigmas'],
    'Dark_map_sigmas_for_largest_diff_pix':['Number of sigmas the dark map differs from the measured dark for the pixel with the largest difference'],
    'Dark_map_retr_param1':['Retrieved value for dark background fitting parameter 1'],
    'Dark_map_retr_param2':['Retrieved value for dark background fitting parameter 2'],
    'Dark_map_retr_param3':['Retrieved value for dark background fitting parameter 3'],
    'Dark_map_retr_param4':['Retrieved value for dark background fitting parameter 4'],
    'Dark_map_retr_param5':['Retrieved value for dark background fitting parameter 5'],
    'Dark_map_retr_param1_uncert':['Uncertainty of retrieved value for dark background fitting parameter 1'],
    'Dark_map_retr_param2_uncert': ['Uncertainty of retrieved value for dark background fitting parameter 2'],
    'Dark_map_retr_param3_uncert': ['Uncertainty of retrieved value for dark background fitting parameter 3'],
    'Dark_map_retr_param4_uncert': ['Uncertainty of retrieved value for dark background fitting parameter 4'],
    'Dark_map_retr_param5_uncert': ['Uncertainty of retrieved value for dark background fitting parameter 5'],


    #Stray light:
    "Stray_light_correction_method":["Stray light correction method"],
    "Stray_light_residual_level":["Estimated average residual stray light level [%]"],
    "Stray_light_before_correction_at_300nm":["Estimated stray light in the signal before correction at 300nm [%]"],
    "Stray_light_before_correction_at_302.5nm":["Estimated stray light in the signal before correction at 302.5nm [%]"],
    "Stray_light_before_correction_at_305nm":["Estimated stray light in the signal before correction at 305nm [%]"],
    "Stray_light_before_correction_at_310nm":["Estimated stray light in the signal before correction at 310nm [%]"],
    "Stray_light_before_correction_at_320nm":["Estimated stray light in the signal before correction at 320nm [%]"],
    "Stray_light_before_correction_at_350nm":["Estimated stray light in the signal before correction at 350nm [%]"],
    "Stray_light_before_correction_at_400nm":["Estimated stray light in the signal before correction at 400nm [%]"],
    "Stray_light_residual_level": ["Estimated average residual stray light level [%]"],

    #Wv change determination:
    "Wv_change_det_num_of_iter":["Number of iterations needed to reach tolerance goal at wavelength change determination"],
    "Wv_change_det_last_cost_function":["Last cost function at wavelength change determination [nm]"],
    "Wv_change_det_rms_of_final_radiometric_difference":["rms of final radiometric difference at wavelength change determination"],
    "Wv_change_det_num_of_pix_not_included":["Number of pixels not included in the wavelength change retrieval"],

    #Retrieved wv change
    "Wv_change_pol_order":["Order of wavelength change polynomial used in spectral fitting"],# Column 19: Order of wavelength change polynomial used in spectral fitting, -1=no wavelength change
    "Wv_change_pol_coeff_order_0":["Retrieved wavelength change, order 0","Wavelength change polynomial coefficient, order 0"],# Column 51: Wavelength change polynomial coefficient, order 0
    "Wv_change_pol_coeff_order_0_uncert":["Uncertainty of wavelength change polynomial coefficient, order 0"],# Column 52: Uncertainty of wavelength change polynomial coefficient, order 0, negative value=not fitted or fitting not successfull
    "Wv_change_pol_coeff_order_1":["Retrieved wavelength change, order 1","Wavelength change polynomial coefficient, order 1"],# Column 53: Wavelength change polynomial coefficient, order 1
    "Wv_change_pol_coeff_order_1_uncert":["Uncertainty of wavelength change polynomial coefficient, order 1"],# Column 54: Uncertainty of wavelength change polynomial coefficient, order 1, negative value=not fitted or fitting not successfull
    "Wv_change_pol_coeff_order_2": ["Retrieved wavelength change, order 2","Wavelength change polynomial coefficient, order 2"], # Column 53: Wavelength change polynomial coefficient, order 1
    "Wv_change_pol_coeff_order_2_uncert": ["Uncertainty of wavelength change polynomial coefficient, order 2"],# Column 54: Uncertainty of wavelength change polynomial coefficient, order 1, negative value=not fitted or fitting not successfull
    "Wv_change_pol_coeff_order_3": ["Retrieved wavelength change, order 3","Wavelength change polynomial coefficient, order 3"], # Column 53: Wavelength change polynomial coefficient, order 1
    "Wv_change_pol_coeff_order_3_uncert": ["Uncertainty of wavelength change polynomial coefficient, order 3"],# Column 54: Uncertainty of wavelength change polynomial coefficient, order 1, negative value=not fitted or fitting not successfull


    #Wv correction:
    "Wv_corr_num_of_pix_not_included":["Number of pixels not included in the wavelength correction"],
    "Wv_corr_mean":["Mean wavelength correction applied [%]"],
    "Wv_corr_std":["Standard deviation of wavelength correction applied [%]"],
    "Wv_corr_min":["Minimum wavelength correction applied [%]"],
    "Wv_corr_max":["Maximum wavelength correction applied [%]"],

    #Wavelength shift
    "Wv_shift": ["Retrieved wavelength shift [nm]","Retrieved wavelength shift from spectral fitting"], # Retrieved wavelength shift [nm], -9=no wavelength change determination
    'Wv_shift_from_corr_data':["Retrieved wavelength shift from level 1 data", "Retrieved wavelength shift from level 2 data"],
    "Wv_shift_expected_by_eff_temp":["Expected wavelength shift based on effective temperature [nm]"],

    #Fitting window info:
    "Fit_window_index":["Fitting window index"],# Column 14: Fitting window index, unique number for each fitting window
    "Fit_window_wv_start":["Starting wavelength of fitting window [nm]"],# Column 15: Starting wavelength of fitting window [nm]
    "Fit_window_wv_end":["Ending wavelength of fitting window [nm]"],# Column 16: Ending wavelength of fitting window [nm]
    "Fit_config_index":["Sum over 2^i with i being a fitting configuration index", "Fitting configuration index"],# Column 20: Sum over 2^i with i being a fitting configuration index, 0=Ring spectrum is fitted, 1=molecular scattering is subtracted before fitting, 2=linear fitting is applied despite non-linear situation, 3=uncertainty in not used in fitting, 4 and 5 decide which reference is used: both 4 and 5 not set uses the synthetic reference spectrum from the calibration file, 4 set and 5 not set uses the theoretical reference spectrum from the calibration file, 4 not set and 5 set uses the measured spectrum with the lowest viewing zenith angle in the sequence, both 4 and 5 set uses a reference spectrum from an external file, 6=level 2 data wavelength change not used in fitting
    "Fit_result_index":["Fitting result index"], # Column 21: Fitting result index: 1,2=no error, >2=error
    "Fit_result_num_of_funct_eval_used": ["Number of function evaluations used"],# Column 22: Number of function evaluations used, 0=linear fitting


    #Fitting polynomials info
    "Fit_pol_coeff_order_0":["Fitting polynomial coefficient, order 0"],# Column 39: Fitting polynomial coefficient, order 0
    "Fit_pol_coeff_order_0_uncert":["Uncertainty of fitting polynomial coefficient, order 0"],# Column 40: Uncertainty of fitting polynomial coefficient, order 0, negative value=not fitted or fitting not successfull
    "Fit_pol_coeff_order_1":["Fitting polynomial coefficient, order 1"],# Column 41: Fitting polynomial coefficient, order 1
    "Fit_pol_coeff_order_1_uncert":["Uncertainty of fitting polynomial coefficient, order 1"],# Column 42: Uncertainty of fitting polynomial coefficient, order 1, negative value=not fitted or fitting not successfull
    "Fit_pol_coeff_order_2":["Fitting polynomial coefficient, order 2"],# Column 43: Fitting polynomial coefficient, order 2
    "Fit_pol_coeff_order_2_uncert":["Uncertainty of fitting polynomial coefficient, order 2"],# Column 44: Uncertainty of fitting polynomial coefficient, order 2, negative value=not fitted or fitting not successfull
    "Fit_pol_coeff_order_3":["Fitting polynomial coefficient, order 3"],# Column 45: Fitting polynomial coefficient, order 3
    "Fit_pol_coeff_order_3_uncert":["Uncertainty of fitting polynomial coefficient, order 3"],# Column 46: Uncertainty of fitting polynomial coefficient, order 3, negative value=not fitted or fitting not successfull
    "Fit_pol_coeff_order_4":["Fitting polynomial coefficient, order 4"],# Column 47: Fitting polynomial coefficient, order 4
    "Fit_pol_coeff_order_4_uncert":["Uncertainty of fitting polynomial coefficient, order 4"],# Column 48: Uncertainty of fitting polynomial coefficient, order 4, negative value=not fitted or fitting not successfull

    #Smoothing polynomials info
    "Smoothing_pol_ord":["Order of smoothing polynomial used in spectral fitting"],# Column 17: Order of smoothing polynomial used in spectral fitting

    #Offset polynomials info
    "Offset_pol_ord":["Order of offset polynomial used in spectral fitting"],# Column 18: Order of offset polynomial used in spectral fitting, -1=no offset
    'Offset_pol_coeff_ord_0': ['Offset polynomial coefficient, order 0'],
    'Offset_pol_coeff_ord_0_uncert_instr': ['Uncertainty of offset polynomial coefficient, order 0, based on instrumental uncertainty'],
    'Offset_pol_coeff_ord_0_uncert_meas': ['Uncertainty of offset polynomial coefficient, order 0, negative value','Uncertainty of offset polynomial coefficient, order 0, based on measured uncertainty'], #Pan format,Blick format
    'Offset_pol_coeff_ord_0_uncert_rms': ['Uncertainty of offset polynomial coefficient, order 0, based on rms'],
    'Offset_pol_coeff_ord_1': ['Offset polynomial coefficient, order 1'],
    'Offset_pol_coeff_ord_1_uncert_instr': ['Uncertainty of offset polynomial coefficient, order 1, based on instrumental uncertainty'],
    'Offset_pol_coeff_ord_1_uncert_meas': ['Uncertainty of offset polynomial coefficient, order 1, negative value','Uncertainty of offset polynomial coefficient, order 1, based on measured uncertainty'], #Pan format,Blick format
    'Offset_pol_coeff_ord_1_uncert_rms': ['Uncertainty of offset polynomial coefficient, order 1, based on rms'],

    # Mean signal
    'Mean_signal': ['Mean value of measured data inside fitting window [same units as measurements]'],

    # Climatological data
    'Climat_station_P_hPa': ['Climatological station pressure [hPa]'],
    'Climat_station_P_mbar': ['Climatological station pressure [mbar]'],
    'Climat_station_T_K': ['Climatological station temperature [K]'],


    #Processing options: Corrected data -> Fitted slant column gases
    'Time_int_off_target_index':['Sum over 2^i, 0=spectra were interpolated in time, 1=spectra are corrected for off-target signal'],


    #Diffuse corrections:
    'Diffuse_corr_NO2':['Diffuse correction applied before fitting at effective fitting wavelength for nitrogen dioxide [%]'],
    'Diffuse_corr_O3':['Diffuse correction applied before fitting at effective fitting wavelength for ozone [%]'],
    'Diffuse_corr_SO2':['Diffuse correction applied before fitting at effective fitting wavelength for sulfur dioxide [%]'],
    'Diffuse_corr_HCHO':['Diffuse correction applied before fitting at effective fitting wavelength for formaldehyde [%]'],

    #------------Slant Columns------------------
    'Sc_res_x1e5':['Unweighted slant column residuals for each pixel inside the fitting window multiplied by 1e5'],
    'Sc_res_norm_weighted_instr_x1e5':['Normalized slant column residuals weighted with instrumental uncertainty for each pixel inside the fitting window multiplied by 1e5'],
    'Sc_res_norm_weighted_meas_x1e5':['Normalized slant column residuals weighted with measured uncertainty for each pixel inside the fitting window multiplied by 1e5'],



    #Ring
    'RING_eff_fit_wv_nm':['Effective Ring fitting wavelength [nm]'],
    'RING_fit_spectrum':['Fitted Ring spectrum'],
    'RING_fit_uncert_instr':['Uncertainty of fitted Ring spectrum based on instrumental uncertainty'],
    'RING_fit_uncert_meas':['Uncertainty of fitted Ring spectrum based on measured uncertainty'],
    'RING_fit_uncert_rms':['Uncertainty of fitted Ring spectrum based on rms'],

    #O3
    'O3_sc':['Ozone slant column amount [Dobson Units]'],
    'O3_sc_uncert_instr': ['Uncertainty of ozone slant column amount [Dobson Units] based on instrumental uncertainty'],
    'O3_sc_uncert_meas': ['Uncertainty of ozone slant column amount [Dobson Units], negative value','Uncertainty of ozone slant column amount [Dobson Units] based on measured uncertainty'], #PanFormat, BlickFormat
    'O3_sc_uncert_rms': ['Uncertainty of ozone slant column amount [Dobson Units] based on rms'],
    'O3_eff_fit_wv_nm':['Effective ozone fitting wavelength [nm]'],
    'O3_climat_eff_H_km':['Climatological effective O3 height [km]'],

    'NO2_sc':['Nitrogen dioxide slant column amount [Dobson Units]'],
    'NO2_sc_uncert_instr': ['Uncertainty of nitrogen dioxide slant column amount [Dobson Units] based on instrumental uncertainty'],
    'NO2_sc_uncert_meas': ['Uncertainty of nitrogen dioxide slant column amount [Dobson Units], negative value','Uncertainty of nitrogen dioxide slant column amount [Dobson Units] based on measured uncertainty'], #PanFormat, BlickFormat
    'NO2_sc_uncert_rms': ['Uncertainty of nitrogen dioxide slant column amount [Dobson Units] based on rms'],
    'NO2_eff_fit_wv_nm':['Effective nitrogen dioxide fitting wavelength [nm]'],
    'NO2_climat_eff_H_km':['Climatological effective NO2 height [km]'],

    'SO2_sc':['Sulfur dioxide slant column amount [Dobson Units]'],
    'SO2_sc_uncert_instr': ['Uncertainty of sulfur dioxide slant column amount [Dobson Units] based on instrumental uncertainty'],
    'SO2_sc_uncert_meas': ['Uncertainty of sulfur dioxide slant column amount [Dobson Units], negative value','Uncertainty of sulfur dioxide slant column amount [Dobson Units] based on measured uncertainty'], #PanFormat, BlickFormat
    'SO2_sc_uncert_rms': ['Uncertainty of sulfur dioxide slant column amount [Dobson Units] based on rms'],
    'SO2_eff_fit_wv_nm':['Effective sulfur dioxide fitting wavelength [nm]'],
    'SO2_climat_eff_H_km':['Climatological effective SO2 height [km]'],

    'HCHO_sc':['Formaldehyde slant column amount [Dobson Units]'],
    'HCHO_sc_uncert_instr': ['Uncertainty of formaldehyde slant column amount [Dobson Units] based on instrumental uncertainty'],
    'HCHO_sc_uncert_meas': ['Uncertainty of formaldehyde slant column amount [Dobson Units], negative value','Uncertainty of formaldehyde slant column amount [Dobson Units] based on measured uncertainty'], #PanFormat, BlickFormat
    'HCHO_sc_uncert_rms': ['Uncertainty of formaldehyde slant column amount [Dobson Units] based on rms'],
    'HCHO_eff_fit_wv_nm':['Effective formaldehyde fitting wavelength [nm]'],
    'HCHO_climat_eff_H_km':['Climatological effective HCHO height [km]'],

    'O2O2_sc':['Oxygen dimer slant column amount [Molecules squared per centimeter to the 5th]'],
    'O2O2_sc_uncert_instr': ['Uncertainty of oxygen dimer slant column amount [Molecules squared per centimeter to the 5th] based on instrumental uncertainty'],
    'O2O2_sc_uncert_meas': ['Uncertainty of oxygen dimer slant column amount [Molecules squared per centimeter to the 5th], negative value','Uncertainty of oxygen dimer slant column amount [Molecules squared per centimeter to the 5th] based on measured uncertainty'], #PanFormat, BlickFormat
    'O2O2_sc_uncert_rms': ['Uncertainty of oxygen dimer slant column amount [Molecules squared per centimeter to the 5th] based on rms'],
    'O2O2_eff_fit_wv_nm':['Effective oxygen dimer fitting wavelength [nm]'],
    'O2O2_climat_eff_H_km':['Climatological effective O2O2 height [km]'],

    'H2O_sc':['Water vapor slant column amount [Centimeter]'],
    'H2O_sc_uncert_instr': ['Uncertainty of water vapor slant column amount [Centimeter] based on instrumental uncertainty'],
    'H2O_sc_uncert_meas': ['Uncertainty of water vapor slant column amount [Centimeter], negative value','Uncertainty of water vapor slant column amount [Centimeter] based on measured uncertainty'], #PanFormat, BlickFormat
    'H2O_sc_uncert_rms': ['Uncertainty of water vapor slant column amount [Centimeter] based on rms'],
    'H2O_eff_fit_wv_nm':['Effective water vapor fitting wavelength [nm]'],
    'H2O_climat_eff_H_km':['Climatological effective H2O height [km]'],



    #---------Vertical columns------------
    "O3_vc":["Ozone vertical column amount [Dobson Units]", "Ozone total vertical column amount [Dobson Units]"], #Ozone vertical column amount [Dobson Units]
    "O3_vc_uncert_meas":["Uncertainty of ozone vertical column amount [Dobson Units]", 'Uncertainty of ozone total vertical column amount [Dobson Units]'], #Uncertainty of ozone vertical column amount [Dobson Units] (based on measured uncertainty)
    "O3_amf":["ozone air mass factor"], #"Direct ozone air mass factor", "Geometrical ozone air mass factor"], #Direct sun ozone air mass factor
    "O3_eff_temp":["Ozone effective temperature [K]"],# -9=not retrieved or retrieval not successfull
    "O3_eff_temp_uncert":["Uncertainty of ozone effective temperature [K]"], #[K], -9=not retrieved or retrieval not successfull

    "NO2_vc":["Nitrogen dioxide vertical column amount [Dobson Units]",'Nitrogen dioxide total vertical column amount [Dobson Units]'], #Nitrogen dioxide vertical column amount [Dobson Units]
    "NO2_vc_uncert_meas":['Uncertainty of nitrogen dioxide vertical column amount [Dobson Units]','Uncertainty of nitrogen dioxide total vertical column amount [Dobson Units]'], #'Uncertainty of nitrogen dioxide vertical column amount [Dobson Units]'
    "NO2_amf":['nitrogen dioxide air mass factor'], #"Geometrical nitrogen dioxide air mass factor", 'Direct nitrogen dioxide air mass factor'], #'Direct sun nitrogen dioxide air mass factor'
    "NO2_eff_temp":["Nitrogen dioxide effective temperature ["],# 'Nitrogen dioxide effective temperature [K], -9=not retrieved or retrieval not successfull'
    "NO2_eff_temp_uncert":['Uncertainty of nitrogen dioxide effective temperature [K]'], #-9=not retrieved or retrieval not successfull'

    "SO2_vc":["Sulfur dioxide vertical column amount [Dobson Units]", "Sulfur dioxide total vertical column amount [Dobson Units]"], #Nitrogen dioxide vertical column amount [Dobson Units]
    "SO2_vc_uncert_meas":['Uncertainty of sulfur dioxide vertical column amount [Dobson Units]', 'Uncertainty of sulfur dioxide total vertical column amount'], #'Uncertainty of nitrogen dioxide vertical column amount [Dobson Units]' (based on measured uncertainty)
    "SO2_amf":['sulfur dioxide air mass factor'], #"Geometrical sulfur dioxide air mass factor",'Direct sulfur dioxide air mass factor', "Geometrical sulfur dioxide air mass factor"], #'Direct sun nitrogen dioxide air mass factor'
    "SO2_eff_temp":["Sulfur dioxide effective temperature ["],# 'Nitrogen dioxide effective temperature [K], -9=not retrieved or retrieval not successfull'
    "SO2_eff_temp_uncert":['Uncertainty of sulfur dioxide effective temperature [K]'], #-9=not retrieved or retrieval not successfull'

    "HCHO_vc":["Formaldehyde vertical column amount [Dobson Units]", "Formaldehyde total vertical column amount [Dobson Units]"],#  Formaldehyde vertical column amount [Dobson Units], -9e99=not fitted or fitting not successfull
    "HCHO_vc_uncert_meas":["Uncertainty of formaldehyde vertical column amount [Dobson Units]", "Uncertainty of formaldehyde total vertical column amount [Dobson Units]"],# Uncertainty of formaldehyde vertical column amount [Dobson Units], negative value=not fitted or fitting not successfull (based on measured uncertainty)
    "HCHO_amf":['formaldehyde air mass factor'],# "Geometrical formaldehyde air mass factor",'Direct formaldehyde air mass factor',"Geometrical formaldehyde air mass factor"],#  Direct sun formaldehyde air mass factor
    "HCHO_eff_temp":["Formaldehyde effective temperature ["],# 'Nitrogen dioxide effective temperature [K], -9=not retrieved or retrieval not successfull'
    "HCHO_eff_temp_uncert":['Uncertainty of formaldehyde effective temperature [K]'], #-9=not retrieved or retrieval not successfull'

    "O2O2_vc":["Oxygen dimer vertical column amount [Dobson Units]", "Oxygen dimer total vertical column amount [Dobson Units]"],
    "O2O2_vc_uncert_meas":["Uncertainty of oxygen dimer vertical column amount [Dobson Units]", "Uncertainty of oxygen dimer total vertical column amount [Dobson Units]"], #(based on measured uncertainty)
    "O2O2_amf":['oxygen dimer air mass factor'], # "Geometrical oxygen dimer air mass factor",'Direct oxygen dimer air mass factor',"Geometrical oxygen dimer  air mass factor"],
    "O2O2_eff_temp":["Oxygen dimer effective temperature ["],
    "O2O2_eff_temp_uncert":['Uncertainty of oxygen dimer effective temperature [K]'],

    "H2O_vc":["Water vapor vertical column amount [Dobson Units]", "Water vapor total vertical column amount [Dobson Units]"],
    "H2O_vc_uncert_meas":["Uncertainty of water vapor vertical column amount [Dobson Units]", "Uncertainty of water vapor total vertical column amount [Dobson Units]"],
    "H2O_amf":['water vapor air mass factor'],
    "H2O_eff_temp":["Water vapor effective temperature ["],
    "H2O_eff_temp_uncert":['Uncertainty of oxygen dimer effective temperature [K]'],

    #Fitting Resuduals -------------------------------------------
    "Fit_res_rms":["rms of unweighted spectral fitting residuals"],
    'Fit_res_norm_rms_weighted_instr':['Normalized rms of fitting residuals weighted with instrumental uncertainty'],
    'Fit_res_norm_rms_weighted_meas':['Normalized rms of fitting residuals weighted with measured uncertainty',"Normalized rms of weighted spectral fitting residuals", "Normalized rms of spectral fitting residuals weighted with measured uncertainty"],

    'Expected_rms_instr': ['Expected rms based on instrumental uncertainty'],
    'Expected_rms_meas': ['Expected rms based on measured uncertainty',"Expected rms based on measured uncertainty only"],
    'Expected_norm_rms_instr':["Expected normalized rms based on instrumental uncertainty only"],
    'Expected_norm_rms_meas':["Expected normalized rms based on measured uncertainty only"],
    'Expected_norm_weighted_rms_instr': ['Expected normalized weighted rms based on instrumental uncertainty',"Expected normalized rms of weighted spectral fitting residuals based on instrumental uncertainty"],
    'Expected_norm_weighted_rms_meas': ['Expected normalized weighted rms based on measured uncertainty',"Expected normalized rms of weighted spectral fitting residuals based on measured uncertainty"],





    #Data quality flags and data quality codes:
    'DQ1_L1_code':['Sum over 2^i using those i, for which the corresponding L1 data quality parameter exceeds the DQ1 limit'],
    'DQ2_L1_code':['Sum over 2^i using those i, for which the corresponding L1 data quality parameter exceeds the DQ2 limit'],
    'DQFlag_L1':['Level 1 data quality flag'],

    'DQ1_L2Fit_code':['Sum over 2^i using those i, for which the corresponding L2Fit data quality parameter exceeds the DQ1 limit'],
    'DQ2_L2Fit_code':['Sum over 2^i using those i, for which the corresponding L2Fit data quality parameter exceeds the DQ2 limit'],
    'DQFlag_L2Fit':['Level 2 Fit data quality flag'],

    'DQ1_L2Tot_O3_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for ozone exceeds the DQ1 limit'],
    'DQ2_L2Tot_O3_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for ozone exceeds the DQ2 limit'],
    'DQFlag_L2Tot_O3': ['L2 data quality flag for ozone'],

    'DQ1_L2Tot_NO2_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for nitrogen dioxide exceeds the DQ1 limit'],
    'DQ2_L2Tot_NO2_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for nitrogen dioxide exceeds the DQ2 limit'],
    'DQFlag_L2Tot_NO2': ['L2 data quality flag for nitrogen dioxide'],

    'DQ1_L2Tot_HCHO_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for formaldehyde exceeds the DQ1 limit'],
    'DQ2_L2Tot_HCHO_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for formaldehyde exceeds the DQ2 limit'],
    'DQFlag_L2Tot_HCHO': ['L2 data quality flag for formaldehyde'],

    'DQ1_L2Tot_SO2_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for sulfur dioxide exceeds the DQ1 limit'],
    'DQ2_L2Tot_SO2_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for sulfur dioxide exceeds the DQ2 limit'],
    'DQFlag_L2Tot_SO2': ['L2 data quality flag for sulfur dioxide'],

    'DQ1_L2Tot_H2O_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for water vapor exceeds the DQ1 limit'],
    'DQ2_L2Tot_H2O_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for water vapor exceeds the DQ2 limit'],
    'DQFlag_L2Tot_H2O': ['L2 data quality flag for water vapor'],

    'DQ1_L2Tot_O2O2_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for oxygen dimer exceeds the DQ1 limit'],
    'DQ2_L2Tot_O2O2_code':['Sum over 2^i using those i, for which the corresponding L2 data quality parameter for oxygen dimer exceeds the DQ2 limit'],
    'DQFlag_L2Tot_O2O2': ['L2 data quality flag for oxygen dimer'],


}


def load_pandora_data(file_path, keyword_dicts=[keywords_pandora], ShowDetectedFileType=True,
                         ShowBindingsErrors=True, ShowLoadTimes=False, ShowHeader=False, Debug=False,
                         RenameColumns=False, AssignKeywords=True):
    # This is a Pandora Files Reader.
    # It reads the file header, detect the number of columns and columns descriptions, and assign them the matched keywords.

    # Inputs: 
    # File path, as string
    # keywords, as dictionary. Example keywords_dict={keyword1:["Column_description1","Column_description2"], keyword2:['...']}

    # Outputs:
    # data, a DataFrame with the file's content.
    # DFF, the detected file format dict. It contains some file data, the assigned columns keywords, column descritpions,
    # and their corresponding data dataframe column indexes.

    # Options:
    # RenameColumns=True/False This will replace the output DF column names (column numbers by default) to the assigned keywords names.

    # from string import rstrip
    # import pandas
    # import sys
    # import os
    # import time

    if Debug:
        ShowLoadTimes = True
        ShowDetectedFileType = True
        ShowBindingsErrors = True
        ShowHeader = True

    data = pandas.DataFrame()
    DFF = {}  # Detected file format dict

    if ShowLoadTimes:
        tstart = time.time()

    if os.path.exists(file_path) == True:

        # Read content
        print("Reading file: " + file_path)

        #Check if the file is compressed:
        if file_path.endswith('.bz2'):
            import bz2
            with bz2.BZ2File(file_path, "rb") as myfile:
                # remove the \n char at the end of every line
                temp=[line.decode('latin1').rstrip() for line in myfile]
        else:
            with open(file_path,"r") as myfile:
                # remove the \n char at the end of every line
                temp=[line.rstrip() for line in myfile]

        filename = os.path.split(file_path)[-1]

        # Split the header of the content:
        # found the line index of the last separator "-----":
        # detect empty lines too.
        found = False
        separatorindex = 0

        for i in range(len(temp)):
            if "-----" in temp[i]:
                found = True
                separatorindex = i
            # print(i)

        if found:
            # separate the header of the rest of the data:
            header = temp[0:separatorindex + 1]
            temp = temp[separatorindex + 1:]
            if ShowHeader:
                print(header)

        else:
            raise Exception("Error while reading file header, separator line (-----) not found.")

        # Build keys for detected file format dict:
        DFF = {"Number_of_columns": -1, \
               "Columns": {}, \
               "Columns_descriptions": {}, \
               "Header": [],
               "Keywords_bindings": {},
               "Metadata": {"Location": "",
                            "Location_full_name": "",
                            "Location_country": "",
                            "Location_latitude": "",
                            "Location_longitude": "",
                            "Location_altitude": ""},
               "Instrument_number": -1,
               "Data_description": "",
               "Level_blick": "",
               "Level_pan": "",
               "Spectrometer_number": -1,
               "Is_blick_format": False,
               "File_name": filename
               }
        # These are the minimum keys. Additional metadata can be added, if they exist.

        # Introduce the header data on DFF:
        DFF["Header"] = header

        # Analyze the header data:
        Number_Of_Columns = 0
        for i in range(len(header)):

            line = header[i]
            if len(line) > 0:
                # Delete commentary, if exist:
                if line[0] == "#":
                    line = line[1:]
                # Delete initial spaces:
                line = line.lstrip()

                # Case Single column description:
                if "Column " in line[0:7] and ":" in line:  # ex: 'Column 6: Integration time [ms]'
                    # split line by ":"
                    linesplitted = line.split(":")  # ex: ['Column 6',' Integration time [ms]']
                    # delete start and end spaces:
                    linesplitted = [linesplitted[j].strip() for j in
                                    range(len(linesplitted))]  # ex: ['Column 6','Integration time [ms]']
                    # Extract col num -1:
                    colnum = [int(linesplitted[0].split(" ")[1]) - 1]  # ex: ['Column','6'] -> [int(6)-1] -> [5]
                    # Extract col description:
                    coldes = linesplitted[1]
                    # Add col info to Detected File Format Dict.
                    DFF["Columns_descriptions"][coldes] = colnum

                    if colnum[0] + 1 > Number_Of_Columns:
                        Number_Of_Columns = colnum[0] + 1
                    continue

                # Case Multiple columns description: (counts, and counts stdev)
                if "Columns " in line[
                                 0:8] and ":" in line:  # ex: Columns 19-2066: Mean over all cycles of raw counts for each pixel',
                    # split line by ":"
                    linesplitted = line.split(
                        ":")  # ex: ['Columns 19-2066',' Mean over all cycles of raw counts for each pixel']
                    # delete start and end spaces:
                    linesplitted = [linesplitted[j].strip() for j in range(len(
                        linesplitted))]  # ex: ['Columns 19-2066','Mean over all cycles of raw counts for each pixel']
                    # Extract starting col num:
                    colrange = linesplitted[0].split(" ")[1]  # ex: '19-2066'
                    colrange = [int(j) - 1 for j in colrange.split("-")]  # ex: [18,2065]
                    colnum = range(colrange[0], colrange[1] + 1)  # ex: [18,19,20, ..., 2065]
                    # Extract col description:
                    coldes = linesplitted[1]
                    # Add col info to Detected File Format Dict.
                    DFF["Columns_descriptions"][coldes] = colnum

                    if colnum[-1] + 1 > Number_Of_Columns:
                        Number_Of_Columns = colnum[-1] + 1
                    continue

                # BlickO format ----------- Metadata--------------
                if "Nominal wavelengths [nm]:" in line:
                    linesplitted = line.split(":")
                    linesplitted = linesplitted[1].strip().split(" ")
                    DFF["Metadata"]["wv"] = [float(wvi) for wvi in linesplitted]
                    continue

                if "File name:" in line:
                    linesplitted = line.split(":")
                    DFF["Metadata"]["File_name"] = linesplitted[1].strip()
                    continue

                if "File generation date:" in line:
                    linesplitted = line.split(":")
                    DFF["Metadata"]["File_generation_date"] = linesplitted[1].strip()
                    continue

                if "Data file version:" in line:
                    linesplitted = line.split(":")
                    DFF["Metadata"]["Data_file_version"] = linesplitted[1].strip()
                    continue

                if "Local principal investigator:" in line:
                    linesplitted = line.split(":")
                    DFF["Metadata"]["PI"] = linesplitted[1].strip()
                    continue

                if "Network principal investigator:" in line:
                    linesplitted = line.split(":")
                    DFF["Metadata"]["Network_PI"] = linesplitted[1].strip()
                    continue

                if "Instrument number:" in line:
                    linesplitted = line.split(":")
                    DFF["Instrument_number"] = linesplitted[1].strip()
                    continue

                if "Data description:" in line:  # This only happens in Blick format files
                    DFF["Is_blick_format"] = True
                    linesplitted = line.split(":")
                    linesplitted = [linesplitted[j].strip() for j in range(len(linesplitted))]
                    # print(linesplitted)
                    DFF["Data_description"] = linesplitted[1]
                    if "level 0 file" in line.lower():
                        DFF["Level_blick"] = "L0"
                        DFF["Level_pan"] = "lev1"
                    if "level 1 file" in line.lower():
                        DFF["Level_blick"] = "L1"
                        DFF["Level_pan"] = "lev2"
                    if "level 2 file" in line.lower() or "level 2 spectral fitting results file" in line.lower():
                        DFF["Level_blick"] = "L2Fit"
                        DFF["Level_pan"] = "lev3a"
                    if 'level 2 total columns file' in line.lower():
                        DFF["Level_blick"] = "L2Tot"
                        DFF["Level_pan"] = "lev3b"
                    continue

                if "Spectrometer number:" in line:
                    linesplitted = line.split(":")
                    DFF["Spectrometer_number"] = linesplitted[1].strip()
                    continue

                if "Short location name:" in line:
                    linesplitted = line.split(':')
                    DFF['Metadata']["Location"] = linesplitted[1].strip()
                    continue

                if "Instrument operation file used:" in line:
                    linesplitted = line.split(':')
                    DFF['Metadata']["OF"] = linesplitted[1].strip()
                    continue

                if "Full location name:" in line:
                    linesplitted = line.split(':')
                    DFF['Metadata']["Location_full_name"] = linesplitted[1].strip()
                    continue

                if "Country of location:" in line:
                    linesplitted = line.split(':')
                    DFF['Metadata']["Location_country"] = linesplitted[1].strip()
                    continue

                if "Location latitude" in line:
                    linesplitted = line.split(':')
                    DFF['Metadata']["Location_latitude"] = linesplitted[1].strip()
                    continue

                if "Location longitude" in line:
                    linesplitted = line.split(':')
                    DFF['Metadata']["Location_longitude"] = linesplitted[1].strip()
                    continue

                if "Location altitude" in line:
                    linesplitted = line.split(':')
                    DFF['Metadata']["Location_altitude"] = linesplitted[1].strip()
                    continue

                if "Data start time" in line:
                    linesplitted = line.split(':')
                    DFF['Metadata']["Data_start_time"] = linesplitted[1].strip()
                    continue

                if "Data end time" in line:
                    linesplitted = line.split(':')
                    DFF['Metadata']["Data_end_time"] = linesplitted[1].strip()
                    continue

                if "Data caveats" in line:
                    linesplitted = line.split(':')
                    DFF['Metadata']["Data_caveats"] = linesplitted[1].strip()
                    continue

                # PanOS format  ----------- Metadata  -------------------
                # First header line
                if "Pandora #" in line:
                    DFF["Metadata"]["File_name"] = os.path.split(file_path)[-1]
                    DFF["Data_description"] = line
                    linesplitted = line.split(" ")
                    DFF["Instrument_number"] = int(linesplitted[1][1:])
                    if "level 1 data" in line.lower():
                        DFF["Level_blick"] = "L0"
                        DFF["Level_pan"] = "lev1"
                    if "level 2 data" in line.lower():
                        DFF["Level_blick"] = "L1"
                        DFF["Level_pan"] = "lev2"
                    if "level 3 data" in line.lower():
                        DFF["Level_blick"] = "L2"
                        DFF["Level_pan"] = "lev3a"
                    if "direct sun retrievals" in line.lower():
                        DFF["Level_blick"] = "L2Tot"
                        DFF["Level_pan"] = "lev3b"
                    continue

                # Second header line:
                if "at " in line and "Lat" in line and "Long" in line:
                    linesplitted = line.split(',')
                    DFF['Metadata']["Location_full_name"] = linesplitted[0][3:].strip()
                    DFF['Metadata']["Location"] = filename.split("_")[1]  # The unique way to get this in pan format is from the filename.
                    DFF['Metadata']["Location_country"] = linesplitted[1].strip()
                    DFF['Metadata']["Location_latitude"] = float(linesplitted[2].partition('Lat ')[-1].replace('\xc2',"").rpartition('\xb0')[0])
                    DFF['Metadata']["Location_longitude"] = float(linesplitted[3].partition('Long ')[-1].replace('\xc2',"").rpartition('\xb0')[0])
                    DFF['Metadata']["Location_altitude"] = float(linesplitted[4].partition(' ')[-1].rpartition('m a.s.l.')[0])
                    continue

                # Fourth header line:
                if "File created with script" in line:
                    d=datetime.datetime.strptime(line.split(' on ')[1],"%a %d %b %Y, %H:%M:%S UT")
                    DFF["Metadata"]["File_generation_date"]=datetime.datetime.strftime(d,iso_date_str)
                    continue

        if Number_Of_Columns == 0:
            raise Exception("Error while reading file header, Number of columns detected = 0.")

        if Debug:
            print("Detected number of columns: "+str(Number_Of_Columns))

        # Save number of columns
        DFF["Number_of_columns"] = Number_Of_Columns
        if AssignKeywords:
            # ---Keywords assignment:---
            # Detect known descriptions in DFF["Columns_descriptions"], assing to them short keywords,
            # and save the assinged keywords and columnumbers into DFF["Columns"]
            for header_description in sorted(DFF["Columns_descriptions"].keys()):  # Loop over the header descriptions
                keywordfound = False
                for keyword_dict in keyword_dicts:  # Loop over the given dictionaries
                    for keyword in keyword_dict.keys():  # Loop over the keywords of the given dictionary
                        descriptions = keyword_dict[keyword]
                        for description in descriptions:  # Loop over the descriptions of a keyword of the given dictionary
                            if description in header_description:
                                keywordfound = True
                                # Save the keyword, and column numbers into DFF["Columns"]
                                DFF["Columns"][keyword] = DFF["Columns_descriptions"][header_description]
                                break
                        if keywordfound:
                            break
                    if keywordfound:
                        break

                if not keywordfound:
                    # If no keyword was found, we use the long description as keyword.
                    if ShowBindingsErrors:
                        print("No keyword found for column description: '" + header_description + "'. Using column description as keyword.")
                    DFF["Columns"][header_description] = DFF["Columns_descriptions"][header_description]

        # Get file contents:

        # Create the column names of the DataFrame in which will be stored the data:
        ColumnIndex = range(Number_Of_Columns)

        # Start parsing data:
        database = []
        for line in temp:
            cells = line.split(" ")

            # if Debug:
            #	print(len(cells))

            if Number_Of_Columns > 3:
                if len(cells) > 3:
                    if cells[4] == "#":  # A commentary row is known because it have a "#" char in the column 5.
                        # print(Cells)
                        continue  # This will go to the next for line.
                else:
                    continue

            # Set type of data: #This method -> 2 seconds
            Cells = []
            for c in cells:
                try:
                    Cells.append(int(c))
                except:
                    try:
                        Cells.append(float(c))
                    except:
                        try:
                            Cells.append(datetime.datetime.strptime(c, iso_date_str2)) #ie: 20210918T112014.8Z
                        except:
                            try:
                                Cells.append(datetime.datetime.strptime(c, iso_date_str)) #ie: 20210918T112014Z
                            except:
                                Cells.append(c)

            database.append(Cells)

        # Convert list of lists to dataframe
        data = pandas.DataFrame(database, index=range(len(database)), columns=ColumnIndex)

        if RenameColumns:
            # Option: rename the column names from numbers to keywords:
            renamedict = {cnum: None for cnum in data.columns.tolist()}
            for cname in DFF["Columns"].keys():
                cnums = DFF["Columns"][cname]
                if len(cnums) > 1:
                    # multiple columns with the same name: modify name.
                    ccount = 1
                    for cnum in cnums:
                        cnamei = cname + "_" + str(ccount)
                        renamedict[cnum] = cnamei
                        ccount = ccount + 1
                else:
                    renamedict[cnums[0]] = cname
            data = data.rename(columns=renamedict)

        # Create the dict Keywords_bindings dictionary, for easy query
        for Description in DFF["Columns_descriptions"].keys():
            for keyword in DFF["Columns"].keys():
                if DFF["Columns_descriptions"][Description] == DFF["Columns"][keyword]:
                    DFF["Keywords_bindings"][keyword] = {}
                    DFF["Keywords_bindings"][keyword]["Header_Description"] = Description
                    DFF["Keywords_bindings"][keyword]["DF_Columns"] = DFF["Columns"][keyword]
                    DFF["Keywords_bindings"][keyword]["Header_Columns"] = [i + 1 for i in DFF["Columns"][keyword]]
                    break

        # Show load times
        if ShowLoadTimes:
            tend = time.time()
            print("Load time: "+str((tend - tstart))+" seconds.")

        if ShowDetectedFileType:
            print("Detected file type:"+str(DFF["Data_description"]))

    else:
        print("file: " + file_path + " does not exist")

    return data, DFF

def get_counts_s(Data, FF):
    #For a raw counts file:
    if 'Counts_scaled' in FF['Columns'].keys() or 'Counts_scaled_1' in FF['Columns'].keys():
        if 'Counts_scaled_1' not in Data.columns.tolist(): #Given a non column renamed DF
            counts_scaled = Data[FF['Columns']['Counts_scaled']].copy()
            scalef = Data[FF['Columns']['Scale_factor'][0]]
            counts = counts_scaled.div(scalef, axis=0)
            it=Data[FF['Columns']['Integration_time'][0]]/1000.0 #ms to seconds
            counts_s = counts.div(it, axis=0)
        else: #Given a column renamed DF
            Counts_scaled_clist = [i for i in Data.columns.tolist() if 'Counts_scaled_' in i]
            counts_scaled = Data[Counts_scaled_clist].copy()
            scalef = Data['Scale_factor']
            counts = counts_scaled.div(scalef, axis=0)
            it = Data['Integration_time'] / 1000.0
            counts_s = counts.div(it, axis=0)
            counts_s.columns=["Counts_s_"+str(i+1) for i in range(len(counts_s.columns.tolist()))]

    #For a corrected counts file:
    elif 'Corr_data_scaled' in FF['Columns'].keys() or 'Corr_data_scaled_1' in FF['Columns'].keys():
        if 'Corr_data_scaled_1' not in Data.columns.tolist():  # Given a non column renamed DF
            counts_s_scaled = Data[FF['Columns']['Corr_data_scaled']]
            scalef = Data[FF['Columns']['Scale_factor'][0]]
            counts_s = counts_s_scaled.div(scalef, axis=0)
        else: #Given a column renamed DF
            Counts_s_scaled_clist = [i for i in Data.columns.tolist() if 'Corr_data_scaled_' in i]
            counts_s_scaled = Data[Counts_s_scaled_clist].copy()
            scalef = Data['Scale_factor']
            counts_s = counts_s_scaled.div(scalef, axis=0)

    else:
        raise Exception("Error in Get_counts_s, File Level not detected or invalid file level.")
    return counts_s

def calc_dt(Data,FF, dref=datetime.datetime(2000, 1, 1, 0, 0, 0)):
    # Calculate the start/stop datetime of each measurement, in both datetime format, and MJD2K format.
    # And add it to the file data:

    m_dat = Data[FF["Columns"]["UT_datetime"][0]].values  # np.array of np.datetime64 objects.
    m_dur = Data[FF["Columns"]["Total_duration"][0]].values  # np.array of floats (Measurement durations in seconds.)

    dt_lst = []
    dt_ini_lst = []
    dt_end_lst = []

    MJD_lst = []
    MJD_ini_lst = []
    MJD_end_lst = []

    isodt_lst = []
    isodt_ini_lst = []
    isodt_end_lst = []

    for i in range(len(m_dat)):
        dur = m_dur[i]  # Duration of mesurement, Seconds
        dat = pandas.Timestamp(m_dat[i])  # Center of measurement, datetime format.
        dat_ini = dat - datetime.timedelta(seconds=dur / 2)  # Start of measurement, datetime format.
        dat_end = dat + datetime.timedelta(seconds=dur / 2)  # End of measurement, datetime format.

        # Datetime lists
        dt_lst.append(dat)
        dt_ini_lst.append(dat_ini)
        dt_end_lst.append(dat_end)

        # ISO str datetime list
        isodt_lst.append(datetime.datetime.strftime(dat, iso_date_str))
        isodt_ini_lst.append(datetime.datetime.strftime(dat_ini, iso_date_str))
        isodt_end_lst.append(datetime.datetime.strftime(dat_end, iso_date_str))

        # MJD2K lists
        MJD_lst.append(float((dat - dref).days) + float((dat - dref).seconds) / 86400.0)
        MJD_ini_lst.append(float((dat_ini - dref).days) + float((dat_ini - dref).seconds) / 86400.0)
        MJD_end_lst.append(float((dat_end - dref).days) + float((dat_end - dref).seconds) / 86400.0)

    if "UT_isodatetime" not in FF["Columns"].keys():
        newcol = Data.columns.max() + 1
        Data[newcol] = isodt_lst
        FF["Columns"]["UT_isodatetime"] = [newcol]
    else:
        Data[FF["Columns"]["UT_isodatetime"][0]] = isodt_lst

    if "UT_isodatetime_ini" not in FF["Columns"].keys():
        newcol = Data.columns.max() + 1
        Data[newcol] = isodt_ini_lst
        FF["Columns"]["UT_isodatetime_ini"] = [newcol]
    else:
        Data[FF["Columns"]["UT_isodatetime_ini"][0]] = isodt_ini_lst

    if "UT_isodatetime_end" not in FF["Columns"].keys():
        newcol = Data.columns.max() + 1
        Data[newcol] = isodt_end_lst
        FF["Columns"]["UT_isodatetime_end"] = [newcol]
    else:
        Data[FF["Columns"]["UT_isodatetime_end"][0]] = isodt_end_lst

    if "UT_MJD2K" not in FF["Columns"].keys():
        newcol = Data.columns.max() + 1
        Data[newcol] = MJD_lst
        FF["Columns"]["UT_MJD2K"] = [newcol]
    else:
        Data[FF["Columns"]["UT_MJD2K"][0]] = MJD_lst

    if "UT_MJD2K_ini" not in FF["Columns"].keys():
        newcol = Data.columns.max() + 1
        Data[newcol] = MJD_ini_lst
        FF["Columns"]["UT_MJD2K_ini"] = [newcol]
    else:
        Data[FF["Columns"]["UT_MJD2K_ini"][0]] = MJD_ini_lst

    if "UT_MJD2K_end" not in FF["Columns"].keys():
        newcol = Data.columns.max() + 1
        Data[newcol] = MJD_end_lst
        FF["Columns"]["UT_MJD2K_end"] = [newcol]
    else:
        Data[FF["Columns"]["UT_MJD2K_end"][0]] = MJD_end_lst

    if "UT_datetime_ini" not in FF["Columns"].keys():
        newcol = Data.columns.max() + 1
        Data[newcol] = dt_ini_lst
        FF["Columns"]["UT_datetime_ini"] = [newcol]
    else:
        Data[FF["Columns"]["UT_datetime_ini"][0]] = dt_ini_lst

    if "UT_datetime_end" not in FF["Columns"].keys():
        newcol = Data.columns.max() + 1
        Data[newcol] = dt_end_lst
        FF["Columns"]["UT_datetime_end"] = [newcol]
    else:
        Data[FF["Columns"]["UT_datetime_end"][0]] = dt_end_lst
    return Data, FF

def calc_dqf(Data, FF, Gas, write_flags=False, folder_out=None):
    #Note: Deprecated, do not use. This is an old script to get the data quality flags of very old pandora data files,
    # captured with the PanOS software. The new Blick Software has specific data quality flags
    print("Calculating quality flags for the pan format file...")

    # If the file is a pan format file, detect the fitting windows from the filename:
    if not FF["Is_blick_format"]:
        fname_in=FF['Metadata']["File_name"]
        if Gas == "NO2" and "FW2" in fname_in:
            fw = "2"
        elif Gas == "O3" and "FW5" in fname_in:
            fw = "5"
        elif Gas == "O3" and "FW6" in fname_in:
            fw = "6"
        else:
            raise Exception("Cannot determine the fitting windows the pandora file. Is it a Vertical Columns Gas File?")

    Gas_fw = Gas + "_fw" + fw

    # Thresholds
    cldTHR = {'NO2_fw2': 0.05, 'O3_fw5': 1., 'O3_fw6': 1.}  # Gas vc uncertainty
    amfTHR = {'NO2_fw2': 7., 'O3_fw5': 5., 'O3_fw6': 5.}  # Amf
    lamTHR = {'NO2_fw2': 0.01, 'O3_fw5': 0.1, 'O3_fw6': 0.1}  # wv_shift
    lamTHRover = {'NO2_fw2': 0.1, 'O3_fw5': 0.2, 'O3_fw6': 0.2}  # wv_shift for overall flags
    wrmsTHR = {'NO2_fw2': 0.005, 'O3_fw5': 0.02, 'O3_fw6': 0.02}  # wrms
    scatTHR = {'NO2_fw2': 0.0004, 'O3_fw5': 0.01, 'O3_fw6': 0.01}  # wrms for scattering detection

    # Columns assignment
    wrmsPAR = FF['Columns']['Fit_res_norm_rms_weighted_meas'][0]  # =5 #wrms
    cPAR = FF['Columns'][Gas + '_vc'][0]  # =6 #Gas vc
    cldPAR = FF['Columns'][Gas + '_vc_uncert_meas'][0]  # =7 #uncert
    amfPAR = FF['Columns'][Gas + '_amf'][0]  # =8 #amf
    errPAR = FF['Columns']['Error_index'][0]  # =9 #error_index
    lamPAR = FF['Columns']['Wv_shift'][0]  # =12 #wv shift

    # Error indexes and width for scat detection.
    minerr = [0, 5, 6]
    majerr = [1, 2, 3, 4]
    oFlgErr = [1]
    wdt = 2
    # lengthHedDesc = 4
    # dataStartLine = 24
    # nRetTab = {}
    pan = str(FF['Instrument_number'])
    print('Pandora: ', pan)
    # nRetTab[pan] = {}
    print('gas and fitting window: ', Gas_fw)
    # nRetTab[pan][gas] = 0

    p = []
    proloc = []

    # for dirN in glob(os.path.join(pthL3b, 'Pandora' + pan + '_' + '*' + '_' + gas + '*_FW' + fw[gas] + '.txt')):
    #    # extract location
    #    proloc.append(dirN.split('\\')[-1].split('_')[1])
    #    # read data
    #    res, A, coldesc = io.read_gendatafile(dirN, iso8601col=[0])
    #    p.append(A)
    #    print 'location read: ', dirN.split('\\')[-1].split('_')[1]
    #    # add number of retrievals
    #    nRetTab[pan][gas] += A.shape[0]

    proloc = [FF["Metadata"]["Location"]]
    A = Data.as_matrix()  # This returns the DF as a numpy array. The equivalent to A of a io.read_gendatafile()
    # The unique exception is that the column 0 are datetimes format.
    p.append(A)

    # flagging
    cldI = []
    amfI = []
    scatI = []
    minerrI = []
    majerrI = []
    wrmsI = []
    lamI = []
    oFlg0 = []
    oFlg1 = []
    oFlg2 = []
    oFlg = []
    for x in xrange(len(p)):
        print('location in process: ', proloc[x])
        # cld
        cldI.append((p[x][:, cldPAR] > cldTHR[Gas_fw]))
        # amf
        amfI.append((p[x][:, amfPAR] > amfTHR[Gas_fw]))
        # wrms
        # wrmsI.append((p[x][:, wrmsPAR] > wrmsTHR[gas]) & -cldI[x] & -amfI[x])
        wrmsI.append((p[x][:, wrmsPAR] > wrmsTHR[Gas_fw]) & ~cldI[x] & ~amfI[x])
        # lam
        # lamI.append((p[x][:, lamPAR] < -lamTHR[gas]) | (p[x][:, lamPAR] > lamTHR[gas]) & -cldI[x] & -amfI[x])
        lamI.append((p[x][:, lamPAR] < -lamTHR[Gas_fw]) | (p[x][:, lamPAR] > lamTHR[Gas_fw]) & ~cldI[x] & ~amfI[x])
        # scat
        wrms = p[x][:, wrmsPAR]
        dwrms = np.zeros_like(wrms)
        cld = p[x][:, cldPAR]
        dcld = np.zeros_like(cld)
        amf = p[x][:, amfPAR]
        damf = np.zeros_like(amf)
        lam = p[x][:, lamPAR]
        dlam = np.zeros_like(lam)
        for i in xrange(-wdt, wdt + 1):
            if i != 0:
                if i < 0:
                    prt = np.append(np.ones(abs(i)) * np.nan, wrms[:i])
                    prtC = np.append(np.ones(abs(i)) * np.nan, cld[:i])
                    prtA = np.append(np.ones(abs(i)) * np.nan, amf[:i])
                    prtL = np.append(np.ones(abs(i)) * np.nan, lam[:i])
                elif i > 0:
                    prt = np.append(wrms[i:], np.ones(abs(i)) * np.nan)
                    prtC = np.append(cld[i:], np.ones(abs(i)) * np.nan)
                    prtA = np.append(amf[i:], np.ones(abs(i)) * np.nan)
                    prtL = np.append(lam[i:], np.ones(abs(i)) * np.nan)
                dwrms = np.column_stack((dwrms, prt - wrms))
                dcld = np.column_stack((dcld, prtC))
                damf = np.column_stack((damf, prtA))
                dlam = np.column_stack((dlam, prtL))
        dwrms = np.delete(dwrms, 0, 1)
        dcld = np.delete(dcld, 0, 1)
        damf = np.delete(damf, 0, 1)
        dlam = np.delete(dlam, 0, 1)
        # scatFLT = (dwrms > scatTHR[gas]) | (dwrms < -scatTHR[gas]) #This method gives RuntimeWarnings due nans.
        scatFLT = (pandas.DataFrame(dwrms).abs() > scatTHR[Gas_fw]).as_matrix()
        # exclude cloud, amf and lambda filter
        for i in xrange(scatFLT.shape[1]):
            # idxOther = (dcld[:, i] > cldTHR[gas]) | (damf[:, i] > amfTHR[gas]) | (dlam[:, i] < -lamTHR[gas]) | (dlam[:, i] > lamTHR[gas])
            # replaced method to avoid RuntimeWarnings due nans.
            idxOther0 = (pandas.Series(dcld[:, i]) > cldTHR[Gas_fw]).values
            idxOther1 = (pandas.Series(damf[:, i]) > amfTHR[Gas_fw]).values
            idxOther2 = (pandas.Series(dlam[:, i]).abs() > lamTHR[Gas_fw]).values
            idxOther = idxOther0 | idxOther1 | idxOther2
            scatFLT[idxOther, i] = False
        scatFLT = np.any(scatFLT, axis=1)
        scatI.append(scatFLT)
        # err
        hh = deepcopy(p[x][:, errPAR]).astype(int)
        errs = np.zeros((len(p[x]), 7), dtype=bool)
        for i in range(7):
            errs[:, i] = ((hh / (2 ** (i + 1))) - np.floor(hh / (2 ** (i + 1)))) > 0
            hh[errs[:, i]] = hh[errs[:, i]] - 2 ** i
        minerrI.append(errs[:, minerr].any(axis=1))
        majerrI.append(errs[:, majerr].any(axis=1))

        # print 'P: ', pan
        print('GAS: ', Gas_fw)
        print('SCATthr:', scatTHR[Gas_fw])
        print('WRMSthr:', wrmsTHR[Gas_fw])
        print('SCAT %: ', (float(sum(scatI[x])) / float(len(scatI[x]))) * 100.)
        print('WRMS %: ', (float(sum(wrmsI[x])) / float(len(wrmsI[x]))) * 100.)

        # define overall quality flags
        # -> CLD or ERR(1)
        oFlg2tmp = errs[:, oFlgErr].any(axis=1) | (p[x][:, cldPAR] > cldTHR[Gas_fw]) | (p[x][:, cldPAR] == -9.)
        oFlg2.append(oFlg2tmp)
        # -> WVL or AMF or SCAT
        oFlg1tmp = (abs(p[x][:, lamPAR]) > lamTHRover[Gas_fw]) | (p[x][:, amfPAR] > amfTHR[Gas_fw]) | scatFLT
        oFlg1.append(oFlg1tmp)
        # make overall quality flag vector
        oFlgtmp = []
        for i in xrange(len(oFlg2tmp)):
            if oFlg2tmp[i] == 1:
                oFlgtmp.append(2)
            elif oFlg1tmp[i] == 1:
                oFlgtmp.append(1)
            else:
                oFlgtmp.append(0)
        oFlg.append(oFlgtmp)
        # make flag file
        if write_flags:
            # flagName = os.path.join(pthL3bQA,'Pandora' + pan + '_' + proloc[x] + '_' + gas + '_FW' + fw[gas] + '_QAflags.txt')
            # if filter_dates:
            #     flagName = os.path.join(folder_out, os.path.splitext(fname_in)[
            #         0] + '_QAflags_' + dmin_iso_center + "_" + dmax_iso_center + '.txt')
            # else:
            flagName = os.path.join(folder_out, os.path.splitext(fname_in)[0] + '_QAflags.txt')
            flagFId = open(flagName, 'w')
            # get header
            # procName = os.path.join(pthL3b, 'Pandora' + pan + '_' + proloc[x] + '_' + gas + '_FW' + fw[gas] + '.txt')
            # procName = fpath
            # procFId = open(procName, 'r')
            # procDmy = procFId.readlines()[:lengthHedDesc + 1]
            # procHed = procDmy[:lengthHedDesc - 1]
            # procSep = procDmy[lengthHedDesc]
            procDmy = FF["Header"]
            procHed = []
            primaryheader = True
            i = 0
            while primaryheader and i < len(procDmy):
                if '----------' not in procDmy[i]:
                    procHed.append(procDmy[i])
                    i = i + 1
                else:
                    primaryheader = False
            procSep = procDmy[i] + "\n"

            # write header and modify first header line
            # for idx, hed in list(enumerate(procDmy)):
            for idx, hed in list(enumerate(procHed)):
                hed = hed.replace('retrievals', 'retrieval data quality flags').replace('\xB0', '°')
                flagFId.write(hed + '\n')
            # write column descriptions
            flagFId.write(procSep)
            flagFId.write('Column 1: UT date and time for center of measurement, yyyymmddThhmmssZ (ISO 8601)\n')
            flagFId.write('Column 2: overall quality index - "0" -> high quality (ready to use), "1" -> medium quality (use with care), "2" -> low quality (do not use)\n')
            flagFId.write('Column 3: cloud flag, - "1" if gas retrieval uncertainty > ' + str(cldTHR[Gas_fw]) + ' DU\n')
            flagFId.write('Column 4: air mass factor (AMF) flag - "1" if AMF > ' + str(amfTHR[Gas_fw]) + '\n')
            flagFId.write('Column 5: normalized root mean square of the weighted spectral fitting residual (WRMS) flag - "1" if WRMS > ' + str(wrmsTHR[Gas_fw]) + ' DU\n')
            flagFId.write('Column 6: wavelength shift flag - "1" if |shift| > ' + str(lamTHR[Gas_fw]) + ' nm\n')
            flagFId.write('Column 7: enhanced data scatter flag - "1" if any(|WRMS_t+i - WRMS_t|) > ' + str(scatTHR[Gas_fw]) + ' DU for i in [-2,-1,1,2]\n')
            flagFId.write('Column 8: weak error flag - "1" if data error does not cause significant change in the magnitude of the retrieved value\n')
            flagFId.write('Column 9: critical error flag - "1" if data error has the capability to change the magnitude of the retrieved value\n')
            flagFId.write(procSep)

            # write data
            for li in xrange(len(cldI[x])):
                # date = getline(procName, li + dataStartLine).split()[0]
                date = datetime.datetime.strftime(A[li, 0], iso_date_str)
                newline = [date, str(oFlg[x][li]), str(cldI[x][li].astype(int)), str(amfI[x][li].astype(int)),
                           str(wrmsI[x][li].astype(int)),
                           str(lamI[x][li].astype(int)), str(scatI[x][li].astype(int)), str(minerrI[x][li].astype(int)),
                           str(majerrI[x][li].astype(int))]
                flagFId.write(" ".join(newline) + '\n')
            print("Flags file saved into: "+str(flagName))
            # clearcache()
            # procFId.close()
            flagFId.close()

        # Update the Data frame with a new entry: The overall quality index:
        # "0" -> high quality (ready to use), "1" -> medium quality (use with care), "2" -> low quality (do not use)

        # Add an extra column to the DataFrame, and update the FF, as if a Blick File had been loaded.
        newc = FF['Number_of_columns']
        Data[newc] = oFlg[x]
        FF["Columns"]["DQFlag_L2Tot_" + str(Gas)] = [newc]
        print("Done.")
        print("")
        return Data, FF

def FF_get_max_col(FF):
    #Get the maximum assigned column of a File Format Dict (FF)
    c=[]
    for i in FF['Columns'].keys():
        c=c+FF['Columns'][i]
    cmax=np.array(c).astype(int)
    return cmax.max()
