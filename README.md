Pandora To Pandas File Reader

Contents:

- An standalone (alternative) pandora file reader which returns the data into a pandas dataframe.

Author: Daniel Santana Díaz
